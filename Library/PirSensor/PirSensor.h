//
// Created by Matteo Favaro on 17/07/16.
//

#ifndef PIRSENSOR_H
#define PIRSENSOR_H

#include <Arduino.h>

class PirSensor {
  uint8_t const pin_;
  uint8_t const calibrationTime_; // in seconds
  unsigned long int warmUpEnd_;
  bool triggered_;

 public:

  PirSensor(uint8_t const pin, uint8_t const calibrationTime);
  bool isTriggered();
  void clearTrigger();
  bool warmUpEnd();
  void warmUpSensor();

};


#endif //PIRSENSOR_H
