//
// Created by Matteo Favaro on 17/07/16.
//

#include "PirSensor.h"


PirSensor::PirSensor(uint8_t pin,
                     uint8_t calibrationTime /* in seconds */ )
    : pin_(pin),
      calibrationTime_(calibrationTime)

{
  pinMode(pin_, INPUT);
}

void PirSensor::warmUpSensor() {
  Serial.println("Warm up started..");
  warmUpEnd_ = millis() + (calibrationTime_ * 1000);
}

bool PirSensor::warmUpEnd() {
  return millis() > warmUpEnd_;
}

bool PirSensor::isTriggered() {
  if(!triggered_)
    triggered_ = digitalRead(pin_);
  return triggered_;
}

void PirSensor::clearTrigger() {
  digitalWrite(pin_,LOW);
  delay(1000);
  triggered_ = false ;
}
