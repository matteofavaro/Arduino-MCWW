//
// Created by Matteo Favaro on 29/07/16.
//

#ifndef ARDUINO_MCWW_SENSORS_H
#define ARDUINO_MCWW_SENSORS_H

//DEBUG
#define SERIAL_DEBUG 0
//#define MESH_DEBUG 0
#define SERIAL_DEBUG_MESSAGE_HELPER 0

//ARDUINO SENSOR PINS
#define LEDREDPIN 2
#define LEDGREENPIN 3
#define LEDWHITEPIN 4
#define PIRSENSORPIN 5

#define cePin 9
#define csnPin 10

#define SERIAL_BUF_SIZE 20

// Options for hardware
#define PIRSENSORCALIBRATIONTIME 60 //seconds
#define LEDBLICKTIME 700 // milliseconds


//NETWORKING
#define DEFAULT_CHANNEL 100//channel Which RF channel to communicate on, 0-125
#define CONFIG_CHANNEL 125//channel Which RF channel to communicate on, 0-125
// --> better choose from 100 to 125 for avoiding collision with wifi 2.4 GHz
//const uint16_t this_node_address = 01111;        // Address of our node in Octal format

//header type for message
/*
 * User types** (1-127) 1-64 will NOT be acknowledged <br>
 * System types** (128-255) 192 through 255 will NOT be acknowledged<br>
 *
 */


#endif //ARDUINO_MCWW_SENSORS_H
