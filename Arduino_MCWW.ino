#include <Arduino.h>

#include "PirSensor.h"
#include "SPI.h"
#include "Leds.h"

#include "printf.h"
#include "RF24.h"
#include "RF24Network.h"
//#include "RF24Mesh.h"
#include "Message.h"
#include "Utility.h"

#include <EEPROM.h>
#include <Utility/Utility.h>
//#include <avr/io.h>
//#include <avr/wdt.h>
#include "SensorManager.h"
#include "Sensors.h"

RF24 radio(cePin, csnPin);
RF24Network network(radio);      // Network uses that radio

PirSensor pir(PIRSENSORPIN, PIRSENSORCALIBRATIONTIME);
Leds leds(LEDREDPIN, LEDGREENPIN, LEDWHITEPIN, LEDBLICKTIME);

MotionSensor motionSensor;
LightLevelSensor lightSensor;

SensorManager sensorManager(network);

char incomingserialBuffer[SERIAL_BUF_SIZE];

//reset function
void(*resetFunc)(void) = 0;
void setup() {

  /*
   * inizializzazione rete
   * stati possibili: 0 = uncofigured
   *  - spostarsi sul canale apposito
   *  - mandarsi un pacchetto al ndirizzo di broadcast se ne riceve 2 rifare la procedura
   *  - mandare al master del canale la propria presenza
   *  - rifare l'invio ogni 5 secondi
   *  - al ricevimento della configurazione salvare i valori in eprom
   *  - spostarsi sul canale di comunicazione generale
   *  - andare in loop
   *
   *  1= stato configurato, inizializzare la rete con i valori da eprom
   *    verifica network con ping al padre
   *    mandare messaggio di ingresso in funzione
   *
   *
   *  Comandi possibili:
   *  - get configured
   *  - getname
   *  - alertnew value
   *  - setuncofigured
   *  - new configuration
   *  - tunronled
   *
   *  Gli stati  sono definiti come coppie nomevalore - valore
   *  per il movimento è : Movement: false/true;
   *
   *  ci saranno 3 led:
   *  rosso: stato non configurato
   *  verde: movimento rilevato
   *  blu: general purpose
   *      - invio dati
   *      - accensione da remoto per ricerca sensore
   *
   *  il bottone di reset sarà configurato come :
   *   - pressione entrata in modalità reset, check ogni 10 secondi con conseguente accensione di un led
   *   - entrata- led blu
   *   - dopo 10 secondi di pressione continua - led verde
   *   - dopo altri 10 secondi - led rosso
   *   - blink per 3 secondi di tutti i led e lancio funzione di reset con
   *     il settaggio dei valori di uncofigured
   *
   */

  Serial.begin(115200);
  delay(5000);

  Serial.println(F("LED'S DANCE"));
  for (int i = 0; i < 5; i++) {
    leds.dance();
  }
  leds.ledAllblink();
  delay(5000);
  leds.ledRedOn();
  randomSeed(analogRead(A0));
  delay(5000);
  Serial.println();
  Serial.println(F("--- Start setup ---"));
  Serial.println();

  #if defined(SERIAL_DEBUG)
  Serial.print(F("MAX payload before fragmentation:"));
  Serial.println(MAX_PAYLOAD_SIZE); //144
  Serial.print(F("MAX MAX_FRAME_SIZE:"));
  Serial.println(MAX_FRAME_SIZE); //144
  #endif
  //RADIOS
  SPI.begin();
  delay(200);
  radio.begin();
  delay(200);
  printf_begin();
  delay(200);
  //adding the sensor for this arduino;
  sensorManager.addSensor(&motionSensor);
  sensorManager.addSensor(&lightSensor);


  //radio.setDataRate(RF24_250KBPS);
  //radio.setChannel(DEFAULT_CHANNEL);
  //delay(100);
  //Serial.print(F("radio Channel:"));
  //Serial.println(radio.getChannel());
  //Serial.print(F("Radio data rate:"));
  //Serial.println(radio.getDataRate());
  //radio.printDetails();
  //leds.ledRedOff();
  //delay(5000);
  //leds.ledRedThreeLongBlink();
  leds.ledRedOff();
  for (int i = 0; i < 5; i++) {
    leds.dance();
  }
  Serial.println(F("LED'S DANCE FINAL"));
  leds.ledAllOn();
  delay(3000);
  leds.ledAllOff();

  sensorManager.begin(network);


  Serial.println(F("--- END SETUP ---"));
  Serial.println();
  Serial.println();


}

void loop() {

  if (Serial.available() > 0) {
    // read the incoming byte:
    Serial.readBytesUntil('\n', incomingserialBuffer, SERIAL_BUF_SIZE);

    // say what you got:
    Serial.print("I received: ");
    Serial.println(incomingByte, DEC);
  }

  sensorManager.checkConfigurationStatus();
  //incoming messages managing
  sensorManager.updateNetwork();
  if(sensorManager.networkAvailable()){
    leds.ledGreenOn();
    sensorManager.receiveMessage();
    delay(1000);
    leds.ledGreenOff();
  }


  // one or more sensors have new values
  if(sensorManager.updateSensorStatus()) {
    leds.ledWhiteOn();
    sensorManager.sendMessages();
    leds.ledWhiteOff();
  }
  else {
    leds.ledWhiteOff();
  }


  /*
  for (int i = 0; i < sensorManager.getSensorsNum(); i++) {
    if (sensorManager.getSensors(i)->hasToSend()) {
      leds.ledWhiteLongBlink();
      sensorManager.getSensors(i)->prepareDataMessageToSend(sensorManager.getMessage());
      if (sensorMessageHelper.sendMessage(sensorManager.getSensors(i),
                                          sensorManager.getSensors(i)->getParent(),
                                          sensorManager.getMessage())) {
        sensorManager.getSensors(i)->messageSent();
      }
    }
  }*/

}


